import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'
import MainLayout from './components/MainLayout.vue'
import Landing from './components/Landing.vue'
import Quiz from './components/Quiz.vue'
import Questions from './components/Questions.vue'
import Finalize from './components/Finalize.vue'

const routes = [
    {
        path: '/',
        name: 'home',
        component: MainLayout,
        children: [
            {
                path: '/',
                component: Landing,
            },
            {
                path: '/quiz',
                name: 'quiz',
                component: Quiz,
            },
            {
                path: '/vragen/:index',
                name: 'questions',
                component: Questions,
            },
            {
                path: '/afronden',
                name: 'finalize',
                component: Finalize,
            }
        ]
    },
]
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})
export default router