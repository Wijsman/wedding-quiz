import { ref, computed } from "vue";
import { firebaseApp } from './firebaseDB';
import {
    getAuth,
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    onAuthStateChanged,
    signOut,
} from "firebase/auth";

const auth = getAuth(firebaseApp);
const authUser = ref(auth.currentUser);
const getUser = computed(() => authUser.value)

const createUser = (email) => {
    createUserWithEmailAndPassword(auth, email, "wedding")
        .then((userCredential) => {
            // Signed in
            console.log("User created");
            authUser.value = userCredential.user
        })
        .catch((error) => {
            console.log(error.code);
            console.log(error.message);
        });
};

const logIn = async (email) => {
    await signInWithEmailAndPassword(auth, email, "wedding")
        .then((userCredential) => {
            // Signed in
            console.log("Logged in");
            authUser.value = userCredential.user
        })
        .catch((error) => {
            console.log(error.code);
            if (error.code === "auth/user-not-found") {
                console.log("User not known, creating new")
                createUser(email)
            }
        });
};

const logOut = async () => {
    await signOut(auth);
    authUser.value = null;
};

onAuthStateChanged(auth, (user) => {
    if (user) {
        authUser.value = user;
    } else {
        authUser.value = null;
    }
});

export const authStateManager = () => ({
    getUser,
    createUser,
    logIn,
    logOut
});