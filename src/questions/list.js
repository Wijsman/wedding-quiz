const userAnswer = [
    {
        index: 6,
        title: "Het koppel is het vaak eens, maar niet altijd. Waarover kunnen Somara en Arvin het nooit eens worden?",
        category: "relationship",
        type: 'mc',
        answers: {
            A: "Wie de vaatwasser inpakt",
            B: "De mate van spicyness in het eten",
            C: "Wie er auto mag rijden",
            D: "Wie de boodschappen moet doen",
        }
    },
    {
        index: 7,
        title: "Hoe oud waren Arvin & Somara toen ze gingen samenwonen?",
        category: "relationship",
        type: 'mc',
        answers: {
            A: "Somara 20, Arvin 21",
            B: "Somara 21, Arvin 21",
            C: "Somara 22, Arvin 22",
            D: "Somara 22, Arvin 23"
        }
    },
    {
        index: 8,
        title: "Waar gaan Arvin & Somara naartoe op huwelijksreis?",
        category: "relationship",
        type: 'open'
    },
    {
        index: 9,
        title: "Wat is Somara's dream job?",
        category: "personal",
        type: 'mc',
        answers: {
            A: "Instagram travel influencer",
            B: "Marketing manager van Chanel",
            C: "Elke job zonder baas",
            D: "Huisvrouw zijn"
        }
    },
    {
        index: 10,
        title: "Wat doet Arvin het liefste, nadat hij voor werk 8 uur achter zijn computer heeft gezeten?",
        category: "personal",
        type: 'mc',
        answers: {
            A: "Op de bank ploffen en bier drinken",
            B: "Buiten een frisse nieus halen",
            C: "Direct gaan sporten",
            D: "Nog 4 uur langer achter de computer zitten"
        }
    },
    {
        index: 11,
        title: "Wie is Somara's Celebrity crush?",
        category: "personal",
        type: 'mc',
        answers: {
            A: "Leonardo di caprio",
            B: "Channing tatum",
            C: "Ryan phillipe",
            D: "Travis Fimmel"
        }
    },
    {
        index: 12,
        title: "Wie is Arvin's Celebrity crush?",
        category: "personal",
        type: 'mc',
        answers: {
            A: "Margot Robbie",
            B: "Scarlet Johansson",
            C: "Nicole Aniston",
            D: "Emma Stone"
        }
    },
    {
        index: 13,
        title: "Wat is Arvins grootste angst?",
        category: "personal",
        type: 'mc',
        answers: {
            A: "Vroeg opstaan",
            B: "Wortelkanaalbehandeling bij de tandarts",
            C: "Dat Somara wordt ontvoerd",
            D: "De vaatwasser uitpakken"
        }
    },
    {
        index: 14,
        title: "Hoeveel cm verschillen Arvin en Somara in lengte?",
        category: "personal",
        type: 'mc',
        answers: {
            A: "Arvin is 5 cm langer",
            B: "Arvin is 8 cm langer",
            C: "Somara is 2 cm langer",
            D: "Ze zijn even groot"
        }
    },
];