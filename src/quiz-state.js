import { ref, computed } from "vue";
import { useStorage } from '@vueuse/core'
import { db } from './firebaseDB';
import {
    collection,
    query,
    where,
    getDocs,
    doc,
    onSnapshot,
    getDoc,
    setDoc,
    addDoc,
    serverTimestamp,
    updateDoc,
} from "firebase/firestore";
import { authStateManager } from "./auth-state";
const user = authStateManager().getUser;

// State
const state = useStorage('quiz-state', {
    questions: [],
    answers: [],
    entriesCount: 0
})

// Questions
const getQuestions = computed(() => {
    return state.value.questions.sort((a, b) => a.data.index - b.data.index);
});
const fetchQuestions = async () => {
    const querySnapshot = await getDocs(collection(db, "questions"));
    state.value.questions = querySnapshot.docs.map((doc) => {
        return {
            id: doc.id,
            data: doc.data(),
        }
    });
};

// Answers
const getAnswers = computed(() => state.value.answers);
const fetchAnswers = async () => {
    const answersRef = collection(db, "user-answers");
    const answerQuery = query(answersRef, where("userId", "==", user.value.uid));
    const querySnapshot = await getDocs(answerQuery);

    state.value.answers = querySnapshot.docs.map((doc) => {
        const data = doc.data();
        return {
            id: doc.id,
            createdAt: data.createdAt,
            question: data.question,
            questionId: data.question.replace('/questions/', ''),
            answer: data.answer,
        }
    });
};

const fetchQuestionAnswer = async (questionRef) => {
    const answersRef = collection(db, "user-answers");
    const answerQuery = query(
        answersRef,
        [
            where("userId", "==", user.value.uid),
            where("question", "==", questionRef)
        ]
    );
    const querySnapshot = await getDocs(answerQuery);

    state.value.answers = querySnapshot.docs.map((doc) => {
        const data = doc.data();
        return {
            id: doc.id,
            answer: data.answer,
        }
    });
};

// Upsert answers
const submitAnswer = async (questionRef, newAnswer) => {
    if (!user.value) {
        console.error('cannot answer question without active user session');
        return
    }

    const userAnswer = state.value.answers.find((ans) => ans.question === questionRef);

    if (userAnswer) {
        // Update
        const answersRef = doc(db, "user-answers", userAnswer.id);
        const answerDoc = await getDoc(answersRef);
        if (answerDoc.exists()) {
            return await updateDoc(
                answersRef,
                'answer',
                newAnswer
            );
        } else {
            console.log('not updated');
            return
        }
    }

    // Create
    console.log('creating answer');
    return await addDoc(collection(db, "user-answers"), {
        answer: newAnswer,
        question: questionRef,
        userId: user.value.uid,
        createdAt: serverTimestamp(),
    });
};

// Entries

const submitQuizEntry = async () => {
    if (!user.value) {
        console.error('cannot submit quiz without active user session');
        return
    }

    // Update entry
    const entryQuery = query(
        collection(db, "entries"),
        where("userId", "==", user.value.uid)
    );
    const querySnapshot = await getDocs(entryQuery);

    if (querySnapshot.docs.length == 1) {
        console.log("submitting quiz entry");
        const fields = {
            submittedAt: serverTimestamp(),
            status: 'submitted'
        };
        return await updateDoc(querySnapshot.docs[0].ref, fields);
    } else {
        console.error('no existing quiz entry found, cannot submit');
    }

};

const createQuizEntry = async () => {
    if (!user.value) {
        console.error('missing active user session');
        return
    }

    const entriesCollection = collection(db, "entries");
    const querySnapshot = await getDocs(
        query(
            entriesCollection,
            where("userId", "==", user.value.uid)
        )
    );

    if (querySnapshot.docs.length == 0) {
        return await addDoc(entriesCollection, {
            startedAt: serverTimestamp(),
            userId: user.value.uid,
            status: 'started'
        });
    } else {
        console.log('found existing quiz entry for user')
    }
};

const getEntriesCount = computed(() => state.value.entriesCount + 1);
const fetchQuizEntries = async () => {
    // Update entry
    const querySnapshot = await getDocs(query(
        collection(db, "entries"),
        where("status", "==", 'submitted')
    ));
    state.value.entriesCount = querySnapshot.docs.length;
};

const addQuestions = async () => {
    if (!user.value) {
        console.error('cannot answer question without active user session');
        return
    }

    const questionList = [
        {
            index: 9,
            title: "Wat is Somara's dream job?",
            category: "personal",
            type: 'mc',
            answers: {
                A: "Instagram travel influencer",
                B: "Marketing manager van Chanel",
                C: "Elke job zonder baas",
                D: "Huisvrouw zijn"
            }
        },
        {
            index: 10,
            title: "Wat doet Arvin het liefste, nadat hij voor werk 8 uur achter zijn computer heeft gezeten?",
            category: "personal",
            type: 'mc',
            answers: {
                A: "Op de bank ploffen en bier drinken",
                B: "Buiten een frisse nieus halen",
                C: "Direct gaan sporten",
                D: "Nog 4 uur langer achter de computer zitten"
            }
        },
        {
            index: 11,
            title: "Wie is Somara's Celebrity crush?",
            category: "personal",
            type: 'mc',
            answers: {
                A: "Leonardo di caprio",
                B: "Channing tatum",
                C: "Ryan phillipe",
                D: "Travis Fimmel"
            }
        },
        {
            index: 12,
            title: "Wie is Arvin's Celebrity crush?",
            category: "personal",
            type: 'mc',
            answers: {
                A: "Margot Robbie",
                B: "Scarlet Johansson",
                C: "Nicole Aniston",
                D: "Emma Stone"
            }
        },
        {
            index: 13,
            title: "Wat is Arvins grootste angst?",
            category: "personal",
            type: 'mc',
            answers: {
                A: "Vroeg opstaan",
                B: "Wortelkanaalbehandeling bij de tandarts",
                C: "Dat Somara wordt ontvoerd",
                D: "De vaatwasser uitpakken"
            }
        },
        {
            index: 14,
            title: "Hoeveel cm verschillen Arvin en Somara in lengte?",
            category: "personal",
            type: 'mc',
            answers: {
                A: "Arvin is 5 cm langer",
                B: "Arvin is 8 cm langer",
                C: "Somara is 2 cm langer",
                D: "Ze zijn even groot"
            }
        },
    ];

    questionList.forEach((q) => {
        // Create
        addDoc(collection(db, "questions"), {
            index: q.index,
            answers: q.answers,
            title: q.title,
            category: q.category,
            type: q.type
        });
    });
};

export const quizStateManager = () => ({
    getQuestions,
    getAnswers,
    getEntriesCount,
    fetchQuestions,
    fetchAnswers,
    fetchQuizEntries,
    submitAnswer,
    submitQuizEntry,
    createQuizEntry,
    addQuestions
});