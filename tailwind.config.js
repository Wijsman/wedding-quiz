const colors = require("tailwindcss/colors");
const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
	mode: "jit",
	content: [
		"./index.html",
		"./src/**/*.{vue,js,ts,jsx,tsx}"
	],
	media: false, // or 'media' or 'class'
	theme: {
		fontFamily: {
			sans: ["Inter", ...defaultTheme.fontFamily.sans],
		},
		extend: {
			colors: {
				gray: colors.gray,
				rose: colors.rose,
				orange: colors.orange,
				fuchsia: colors.fuchsia,
				lime: colors.lime,
				amber: colors.amber,
				sky: colors.sky,
			},
		},
	},
	variants: {
		extend: {},
	},
	plugins: [require("@tailwindcss/forms"), require("@tailwindcss/aspect-ratio")],
	corePlugins: {
		animation: false,
		float: false,
	},
};